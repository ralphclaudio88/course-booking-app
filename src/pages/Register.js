import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');
	const [ mobileNumber, setMobileNumber ] = useState('');

	// State to determine whether submit button is enabled or not
	const [ isActive, setIsActive ] = useState(false);

	// Check if values are successfully binded
	/*console.log(email);
	console.log(password1);
	console.log(password2);*/

	// Function to simulate user registration
	function registerUser(e){

		// Prevents page redirection via form submission
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,{
        	method : "POST",
        	headers : {
        		"Content-Type" : "application/json"
        	},
        	body : JSON.stringify({
    		    email : email
        	})
        })
        .then(res => res.json())
        .then(data => {
        	console.log(data)
        	
        	if(data){
        		Swal.fire({
        		  icon: 'error',
        		  title: 'Duplicate email found',
        		  text: 'Please provide a different email.'
        		})
        	} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
		        	method : "POST",
		        	headers : {
		        		"Content-Type" : "application/json"
		        	},
		        	body : JSON.stringify({
		        		firstName : firstName,
		    		    lastName : lastName,
		    		    email : email,
		    		    mobileNo : mobileNumber,
		    		    password : password1
		        	})
		        })
        		Swal.fire({
        		  icon: 'success',
        		  title: 'Registration Successful!',
        		  text: 'Welcome to Zuitt!'
        		})
        		// Clear input fields
        		setFirstName('');
        		setLastName('');
        		setEmail('');
        		setPassword1('');
        		setPassword2('');
        		setMobileNumber('');

        		navigate("/login")
        	}
        })

	}

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match
		if((email !== "" && password1 !== "" && password2 !== "" 
			&& firstName !== "" && lastName !== "" && mobileNumber !== "")
			&& (password1 === password2) && (!isNaN(mobileNumber))){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2, firstName, lastName, mobileNumber]);

	return(
		(user.id) ?
    		<Navigate to="/courses" />
    	:
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="userFirstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter first name"
					value={firstName}
					onChange={e=>setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="userLastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter last name"
					value={lastName}
					onChange={e=>setLastName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e=>setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">We'll never share your email with anyone.</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter a password"
					value={password1}
					onChange={e=>setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Confirm Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Confirm your password"
					value={password2}
					onChange={e=>setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="userMobileNumber">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter mobile number"
					minLength={11}
					maxLength={11}
					value={mobileNumber}
					onChange={e=>setMobileNumber(e.target.value)}
					required
				/>
			</Form.Group>


			{/* Conditional Rendering for submit button based on isActive state */}
			{
				isActive ?
				<Button variant="primary" type="submit" id="submitBtn" className="mt-2">Register</Button>
				:
				<Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled>Register</Button>
			}
		</Form>

	)
}